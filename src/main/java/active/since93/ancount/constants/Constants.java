package active.since93.ancount.constants;

/**
 * Created by myzupp on 15-02-2016.
 */
public class Constants {

    // Activities status
    public static boolean STATUS_MAIN_ACTIVITY = false;
    public static boolean STATUS_ANALYSIS_ACTIVITY = false;
    public static boolean STATUS_HISTORY_ACTIVITY = false;

    //Broadcast message
    public static final String UPDATE_ACTIVITIES = "active.since93.UPDATE_ACTIVITIES";
}
